from uuid import uuid4

import pytest

from helmholtz_cloud_agent import messages


def make_target_entity_v1() -> messages.TargetEntityV1:
    return messages.TargetEntityV1(group_urn_target=None, user_id_target=uuid4())


class TestHCAMessageValidation:
    @staticmethod
    def test_target_entity_validation() -> None:
        # valid when user_id_target is set
        messages.TargetEntityV1.model_validate(make_target_entity_v1().model_dump())

        # valid when group_urn_target is set
        messages.TargetEntityV1.model_validate(
            make_target_entity_v1().model_dump()
            | {
                "group_urn_target": "urn:geant:example.com:group:test",
                "user_id_target": None,
            }
        )

        # invalid when neither user_id_target nor group_urn_target are set
        with pytest.raises(
            ValueError,
            match="Either group_urn_target or user_id_target must be set",
        ):
            messages.TargetEntityV1.model_validate(
                make_target_entity_v1().model_dump()
                | {
                    "group_urn_target": None,
                    "user_id_target": None,
                }
            )

        # invalid when both user_id_target nor group_urn_target are set
        with pytest.raises(
            ValueError,
            match="Either group_urn_target or user_id_target must be set",
        ):
            messages.TargetEntityV1.model_validate(
                make_target_entity_v1().model_dump()
                | {
                    "group_urn_target": "urn:geant:example.com:group:test",
                    "user_id_target": uuid4(),
                }
            )

        # invalid when group_urn_target has wrong format
        with pytest.raises(
            ValueError,
            match="group_urn_target has invalid format",
        ):
            messages.TargetEntityV1.model_validate(
                make_target_entity_v1().model_dump()
                | {
                    "group_urn_target": "invalid",
                    "user_id_target": None,
                }
            )
