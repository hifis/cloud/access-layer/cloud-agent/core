import re
from typing import Any

from pydantic import (
    UUID4,
    BaseModel,
    ConfigDict,
    Field,
    field_validator,
    model_validator,
)
from pydantic.annotated_handlers import GetJsonSchemaHandler
from pydantic.json_schema import JsonSchemaValue
from pydantic_core import CoreSchema

GROUP_URN_PATTERN = (
    r"^urn:geant:(([a-zA-Z0-9\._-]+:)+)group:(([a-zA-Z0-9\._-]+:)*)([a-zA-Z0-9\._-]+)$"
)


class TargetEntityV1(BaseModel):
    model_config = ConfigDict(
        title="TargetEntityV1", json_schema_extra={"$id": "TargetEntityV1"}
    )
    group_urn_target: str | None
    user_id_target: UUID4 | None

    @model_validator(mode="after")
    def must_be_either_personal_or_group_resource(
        self: "TargetEntityV1",
    ) -> "TargetEntityV1":
        if ((self.group_urn_target is None) and (self.user_id_target is None)) or (
            (self.group_urn_target is not None) and (self.user_id_target is not None)
        ):
            msg = "Either group_urn_target or user_id_target must be set"
            raise ValueError(msg)

        return self

    @field_validator("group_urn_target")
    @classmethod
    def must_contain_valid_group_urn(
        cls: type["BaseModel"], group_urn_target: str
    ) -> str:
        if group_urn_target is None:
            return None
        if re.match(GROUP_URN_PATTERN, group_urn_target):
            return group_urn_target
        else:
            msg = "group_urn_target has invalid format"
            raise ValueError(msg)


class ResourceDeallocateV1(BaseModel):
    __hca_handler__ = "resource_deallocate_v1"
    model_config = ConfigDict(
        title="ResourceDeallocateV1", json_schema_extra={"$id": "ResourceDeallocateV1"}
    )
    id: str = Field(..., title="Id")


class ResourceAllocateV1(BaseModel):
    __hca_handler__ = "resource_allocate_v1"
    model_config = ConfigDict(
        title="ResourceAllocateV1", json_schema_extra={"$id": "ResourceAllocateV1"}
    )
    type: str = Field(..., title="Resource Type")
    specification: dict[str, Any] = Field(..., title="Resource Specification")
    target_entity: TargetEntityV1 = Field(..., title="Target Entity Specification")

    @classmethod
    def __get_pydantic_json_schema__(
        cls, core_schema: CoreSchema, handler: GetJsonSchemaHandler, /
    ) -> JsonSchemaValue:
        json_schema = handler(core_schema)
        json_schema = handler.resolve_ref_schema(json_schema)
        json_schema["examples"] = [
            {
                "target_entity": {
                    "user_id_target": "bd2dc138-e63a-46d4-a042-3c6b7d313b8f",
                    "group_urn_target": None,
                },
                "type": "GroupStorageResourceSpecV1",
                "specification": {
                    "desired_name": "MySpecialFolderName",
                    "quota": {"unit": "GB", "value": 100},
                },
            },
            {
                "target_entity": {
                    "user_id_target": None,
                    "group_urn_target": "urn:geant:example.com:group:test",
                },
                "type": "ComputeResourceSpecV1",
                "specification": {
                    "ram": {"unit": "GB", "value": 16},
                    "cpu": {"unit": "cores", "value": 4},
                    "storage": {"unit": "GB", "value": 100},
                },
            },
        ]
        return json_schema


class ResourceUpdateV1(BaseModel):
    __hca_handler__ = "resource_update_v1"
    model_config = ConfigDict(
        title="ResourceUpdateV1", json_schema_extra={"$id": "ResourceUpdateV1"}
    )
    id: str = Field(..., title="Id")
    type: str = Field(..., title="Type")
    specification: dict[str, Any] = Field(..., title="Resource Specification")
    target_entity: TargetEntityV1 = Field(..., title="Target Entity Specification")


class ResourceCreatedV1(BaseModel):
    model_config = ConfigDict(
        title="ResourceCreatedV1", json_schema_extra={"$id": "ResourceCreatedV1"}
    )
    id: str = Field(..., title="Id")


class ResourceUpdatedV1(BaseModel):
    model_config = ConfigDict(
        title="ResourceUpdatedV1", json_schema_extra={"$id": "ResourceUpdatedV1"}
    )
    id: str = Field(..., title="Id")


class ResourceDeallocatedV1(BaseModel):
    model_config = ConfigDict(
        title="ResourceDeallocatedV1",
        json_schema_extra={"$id": "ResourceDeallocatedV1"},
    )
    id: str = Field(..., title="Id")


class ErrorV1(BaseModel):
    model_config = ConfigDict(title="ErrorV1", json_schema_extra={"$id": "ErrorV1"})
    type: str = Field(..., title="Type")
    message: str = Field(..., title="Error Message")


class GenericResponseV1(BaseModel):
    model_config = ConfigDict(
        title="GenericResponseV1", json_schema_extra={"$id": "GenericResponseV1"}
    )
    successful: bool = Field(..., title="")
    message: str = Field(..., title="Error Message")


class PingV1(BaseModel):
    model_config = ConfigDict(title="PingV1", json_schema_extra={"$id": "PingV1"})
    message: str = Field(..., title="Message")


class UserDeprovisionV1(BaseModel):
    model_config = ConfigDict(
        title="UserDeprovisionV1", json_schema_extra={"$id": "UserDeprovisionV1"}
    )
    __hca_handler__ = "user_deprovision_v1"
    id: str


class UserDeprovisionedV1(BaseModel):
    model_config = ConfigDict(
        title="UserDeprovisionedV1", json_schema_extra={"$id": "UserDeprovisionedV1"}
    )
    id: str
