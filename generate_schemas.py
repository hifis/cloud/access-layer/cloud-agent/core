import inspect
import json
from pathlib import Path

from helmholtz_cloud_agent import messages

for name, clazz in inspect.getmembers(messages, inspect.isclass):
    if clazz.__module__ == "helmholtz_cloud_agent.messages.messages":
        with Path(f"schemas/{name}.schema.json").open(mode="w") as f:
            json.dump(clazz.model_json_schema(), f, indent=2)
